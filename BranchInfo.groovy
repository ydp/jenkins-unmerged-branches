package gitfetch;

public class BranchInfo {
	String repoUrl;
	String branch;
	
	public String toString() {
		return this.repoUrl+", "+this.branch;
	}
}
