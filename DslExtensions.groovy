package gitfetch

import javaposse.jobdsl.dsl.Job;
import javaposse.jobdsl.dsl.WithXmlAction;

import javaposse.jobdsl.dsl.helpers.publisher.PublisherContext;

class DslExtensions {
	static {
		Job.metaClass.setAntTargetTo = { String newTarget ->
			configure {project ->
		        if (!project.builders.empty ) {
		            def builders = project.builders[0];
					def antBuilderNode = builders.'hudson.tasks.Ant'[0]
					antBuilderNode.targets[0].value = newTarget
		        }
			};
		}

		PublisherContext.metaClass.gitCreateTag = { boolean _pushMerge, boolean _pushOnlyIfSuccess, String _configVersion,  String _targetRepoName, String _tagName, boolean _createTag, boolean _updateTag ->
			def nodeBuilder = new NodeBuilder()

			Node gitPublisherNode = nodeBuilder.'hudson.plugins.git.GitPublisher' {
				pushMerge _pushMerge
				pushOnlyIfSuccess _pushOnlyIfSuccess
				configVersion _configVersion
				tagsToPush {
					'hudson.plugins.git.GitPublisher_-TagToPush' {
						targetRepoName _targetRepoName
						tagName _tagName
						createTag _createTag
						updateTag _updateTag
					}
				}
			}

			publisherNodes << gitPublisherNode
		}

	}
	
	static install() {
	}
}
