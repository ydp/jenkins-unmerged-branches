package gitfetch


public class FetchProjectGitBranches {
	
	private ProjectRepo projects = new ProjectRepo();
	private GitClient gitClient = new GitClient();

	Collection<BranchInfo> getProjectBranches( ProjectInfoProvider project, String branch ) {
		def repo = project.getRepoUrl();

		def destDir = fetchRepo( project.name, repo );
		
		return gitClient.listUnmergedBranches( destDir, branch ).collect { branchName ->
			new BranchInfo( repoUrl: repo, branch: branchName );
		}
	}

	def fetchRepo( projectName, url ) {
		def fetchDir = projects.prepareFetchDir( projectName );
		
		gitClient.fetchRepo( fetchDir, url );
		
		fetchDir;
	}
}