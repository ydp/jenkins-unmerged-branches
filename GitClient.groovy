package gitfetch

import java.io.File;

class GitClient {
	
	def fetchRepo( File repoDir, String url ) {
		def fetchCommand = getFetchCommand( repoDir, url )
		repoDir.mkdirs();

		try {
			resultLines(fetchCommand, repoDir )
		} catch( x ) {
			repoDir.deleteDir();
			throw x;
		}
	}
	
	Collection<String> listUnmergedBranches( File repoDir, String branch ) {
		def branchesCommand = "git branch --no-merge ${branch}"
		
		return resultLines(branchesCommand, repoDir).collect() {
			it.startsWith('*') ? it[1..-1].trim() : it;
		};
	} 

	protected String getFetchCommand( File fetchDir, String url ) {
		def gitDir = new File( fetchDir, ".git" );
		if ( gitDir.exists() ) {
			"git fetch --quiet -p origin *:*"
		} else {
			"git clone --quiet --bare ${url} \"${fetchDir.absolutePath}"+File.separator+".git\""
		}
	}

	
	protected String[] resultLines(String command, File workDir) {
	    def process = command.execute( null, workDir )
	    def inputStream = process.getInputStream()
	    def gitOutput = ""

	    while(true) {
	      int readByte = inputStream.read()
	      if (readByte == -1) break // EOF
	      byte[] bytes = new byte[1]
	      bytes[0] = readByte
	      gitOutput = gitOutput.concat(new String(bytes))
	    }
	    process.waitFor();

	    if (process.exitValue() == 0) {
			def lines = gitOutput.split('\n')
			def trimmedLines = lines.collect { it.trim() }
			def nonEmptyLines = trimmedLines.grep{ !it.empty };
			
	        nonEmptyLines
	    } else {
            String errorText = process.errorStream.text?.trim()
            throw new Exception("Error executing command: $command -> $errorText")
	    }
	}
}
