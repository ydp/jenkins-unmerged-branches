package gitfetch;
import jenkins.model.Jenkins;

public class JenkinsProjectInfoProvider implements ProjectInfoProvider {
	String projectName;

	public String getName() {
		return projectName;
	}

	public String getRepoUrl() {
		def job = Jenkins.instance.getItem( projectName )
		def userRemoteConfig = job.scm.userRemoteConfigs[0];

		userRemoteConfig.url
	}
}