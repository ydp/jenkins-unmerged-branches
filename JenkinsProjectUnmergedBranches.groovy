package gitfetch

class JenkinsProjectUnmergedBranches {
	static get( String project, String branch ) {
		JenkinsProjectInfoProvider jpip = new JenkinsProjectInfoProvider( projectName: project );
		def fetcher = new FetchProjectGitBranches()
		
		fetcher.getProjectBranches( jpip, branch )
	}
	static get( String project, String url, String branch ) {
		SimpleProjectInfoProvider spip = new SimpleProjectInfoProvider( projectName: project, repoUrl: url )
		def fetcher = new FetchProjectGitBranches()
		
		fetcher.getProjectBranches( spip, branch )
	}
	static getProjectRepo( String project ) {
		JenkinsProjectInfoProvider jpip = new JenkinsProjectInfoProvider( projectName: project );
		
		jpip.getRepoUrl();
	}
	/**
	 * Filtruje tylko branche (bug|story)/(LITERY)-(CYFRY)
	 * @param branchInfo
	 * @return
	 */
	static def stdBranchNameFilter( def branchInfo ) {
		def name = branchInfo.branch;
		return name.matches( "(bug|story)/[A-Z]+-[0-9]+" );
	}
	
	static convertBranchNameForJenkins( String branch ) {
		branch.replaceAll( "[\"#%<>[\\]^`{|}~/;:?]!@\$&*]", "_")
	}
}
