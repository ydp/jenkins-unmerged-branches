package gitfetch

public interface ProjectInfoProvider {
	String getName();
	String getRepoUrl();
}