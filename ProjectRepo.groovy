package gitfetch

import java.io.File;
import java.io.FileFilter;

class ProjectRepo {
	static String GITFETCH_PREFIX = "gitfech."
	static String PROJECTDIR_PREFIX = "fetchproject-";
	
	public File prepareFetchDir( projectName ) {
		def fetchDir = getFetchDirectory();

		File projectDir = new File( fetchDir, PROJECTDIR_PREFIX+projectName );
		cleanUpFetchDir( fetchDir, projectDir );

		return projectDir;
	}

	def getFetchDirectory() {
		def tempDir = System.getProperty(GITFETCH_PREFIX+"tmpdir")?:System.getProperty( "java.io.tmpdir" );

		return new File( tempDir );
	}

	def cleanUpFetchDir( File fetchDir, File [] exceptions ) {
		def freeSpace = fetchDir.freeSpace;
		def freeSpaceLimit = getFreeSpaceThreshold();

		if ( freeSpace<freeSpaceLimit ) {
			def candidates = getDeletionCandidates( fetchDir, exceptions ) as Stack<File>;

			while( fetchDir.freeSpace<freeSpaceLimit && !candidates.empty ) {
				def dir = candidates.pop()
				dir.deleteDir();
				if ( dir.exists() ) {
					dir.renameTo( new File(dir.parent, "___"+dir.name) );
				}
			}
		}
	}

	class FetchProjectDirFilter implements FileFilter {
		public boolean accept( File file ) {
			return file.directory && file.name.startsWith( PROJECTDIR_PREFIX );
		}
	}

	def getDeletionCandidates( File fetchDir, File [] exceptions ) {
		def projectFiles = fetchDir.listFiles( new FetchProjectDirFilter() )
		File [] filterdProjectFiles = exceptions ? projectFiles-exceptions : projectFiles;
		
		filterdProjectFiles
		
	}

	def getFreeSpaceThreshold() {
		def limitString = System.getProperty( GITFETCH_PREFIX+"freespacelimit" )?:(3l*1024*1024*1024 as String);

		limitString as Long
	}

}
